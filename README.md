1. Bal oldalon a "Source" menüt kell választani,
2. A titeket érintő fájlok az en_US és hu_HU mappában vannak,
3. Mind a két mappában egy-egy paintcocktail.po fájlt fogtok találni(rákattintásra megnyílik a fájl),
4. A jobb felső sarokban találtok egy "Edit" gombot arra kell kattintani és akkor megjelenik az online szerkesztő felület,
5. Amikor szerkesztitek az adott fájlokat, mindíg az msgstr utáni idézőjelek közé kell írnotok, az msgid utánit nem szabad módosítani,
6. Ha történik valamilyen módosítás akkor a jobb alsó sarkában a szerkesztőnek a "Commit" és "View diff" gombok elérhetővé válnak. A "View diff" gombbal meg tudjátok nézni a változtatásokat, a "Commit" gombbal pedig rögzíteni tudjátok azt.
7. Ha megnyomjátok a "Commit" gombot megjelenik egy feluigró ablak, amibe érdemes beírni, hogy ki-mit-mikor-miért módosított/egészített ki.
8. Ezzel elkészült a változtatás és bekerült a verziókövetőbe, valamint megjeleníti, hogy mi változott.

Ha Bitbucket felületén történt módosítás a paintcocktail Team által: 

Commit után Pull Requestet kell küldeni nekünk és reviewer-nek Pesti György, Rapi Ádám, illetve Székelyi György legyen megadva.

Általunk kódban történt változtatás esetén:

paintcocktail-i18n szövegeket tartalmazó csomagot a paintcocktail projecktel összelinkelni a következőképp kell: 

i18n projektben npm link paranccsal létrehozunk egy virtuális linket, amit utána a paintcocktail projectben, npm link paintcocktail-i18n paranccsal belinkelünk, így minden változtatás amit az i18n fájljaiban változtatunk, megváltozik a paintcocktail project node modules mappájában lévő i18n csomagban is változik.
Minden npm install után meg kell ismételjük ezt a folyamatot!!!
